Discord Websocket Library
=========================

Still a WIP, but this library will help your application subscribe to and parse the Discord websocket API.

Instructions
============

Copy your `.env.example` file to `.env`. You will then need to add your Discord bot token.

Once that is done you can do something like this:
```php
<?php

require 'vendor/autoload.php';

use judahnator\DiscordWebsocket\Models\Author;
use judahnator\DiscordWebsocket\Models\Message;

$discord = new \judahnator\DiscordWebsocket\DiscordEventLoop();

$discord->on('MESSAGE_CREATE', function(Author $author, Message $message) {
    echo "Received message from {$author->username}: {$message->content}".PHP_EOL;
});

$discord->run();
```

I have most of the big events hooked up, so you can pass those to the "on" function. For a list of the available gateway events see [this page in the documentation](https://discordapp.com/developers/docs/topics/gateway#commands-and-events-gateway-events).

The second argument to the "on" function utilizes PHP-DI to load the dependencies, so as long as you type-hint your variables you can pass whatever you like in whatever order you like. 

Feel free to help contribute. Just fork the repo, make your changes, and submit a PR.