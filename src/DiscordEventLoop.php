<?php

namespace judahnator\DiscordWebsocket;


use DI\Container;
use function DI\factory;
use function judahnator\DiscordWebsocket\Functions\env;
use judahnator\DiscordWebsocket\Models\Author;
use judahnator\DiscordWebsocket\Models\Channel;
use judahnator\DiscordWebsocket\Models\Emoji;
use judahnator\DiscordWebsocket\Models\Guild;
use judahnator\DiscordWebsocket\Models\Message;
use judahnator\WebsocketEventLoop\WebsocketEventLoop;

class DiscordEventLoop extends WebsocketEventLoop
{

    private $eventList = [];
    private $hasSentIdentity = false;

    public function __construct()
    {
        parent::__construct('wss://gateway.discord.gg', ['timeout' => 3600]);
    }

    protected function doEvent(string $event, Container $container): void
    {
        foreach ($this->eventList[$event] ?? [] as $callback) {
            $container->call($callback);
        }
    }

    final public function getEventCallbackArgument()
    {
        return json_decode(parent::getEventCallbackArgument(), true) ?: parent::getEventCallbackArgument();
    }

    final public function loopSetup(): void
    {
        $this->duringEvent(function ($message) {
            if (!$message) {
                return;
            }
            switch ($message['op']) {

                case 0:
                    $container = new Container();
                    switch ($message['t']) {

                        case 'CHANNEL_CREATE':
                        case 'CHANNEL_DELETE':
                        case 'CHANNEL_UPDATE':
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );
                            break;

                        case 'CHANNEL_PINS_UPDATE':
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['channel_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );
                            break;

                        case 'GUILD_CREATE':
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['id']);
                                })->parameter('message', $message)
                            );
                            break;

                        case 'MESSAGE_CREATE':
                        case 'MESSAGE_UPDATE':
                            $container->set(
                                Message::class,
                                factory(function ($message) {
                                    return new Message(json_decode(json_encode($message['d'])));
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Author::class,
                                factory(function ($message) {
                                    return Author::find($message['d']['author']['id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['channel_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );

                            break;

                        case 'MESSAGE_DELETE':
                            $container->set(
                                Message::class,
                                factory(function ($message) {
                                    return Message::find($message['d']['channel_id'], $message['d']['id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['channel_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );
                            break;

                        case 'MESSAGE_REACTION_ADD':
                        case 'MESSAGE_REACTION_REMOVE':
                            $container->set(
                                Emoji::class,
                                factory(function ($message) {
                                    return new Emoji(
                                        $message['d']['emoji']['name'],
                                        $message['d']['emoji']['id'],
                                        $message['d']['emoji']['animated']
                                    );
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Author::class,
                                factory(function ($message) {
                                    return Author::find($message['d']['user_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Message::class,
                                factory(function ($message) {
                                    return Message::find($message['d']['channel_id'], $message['d']['message_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['channel_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );
                            break;

                        case 'READY':
                            // Nothing to do here
                            break;

                        case 'TYPING_START':
                            $container->set(
                                Author::class,
                                factory(function ($message) {
                                    return Author::find($message['d']['user_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Channel::class,
                                factory(function ($message) {
                                    return Channel::find($message['d']['channel_id']);
                                })->parameter('message', $message)
                            );
                            $container->set(
                                Guild::class,
                                factory(function ($message) {
                                    return Guild::find($message['d']['guild_id']);
                                })->parameter('message', $message)
                            );
                            break;

                        default:
                            print_r($message);
                            die($message['t'] . PHP_EOL);

                    }
                    $this->doEvent($message['t'], $container);
                    break;

                case 10:
                    echo "Received hello, responding with heartbeat." . PHP_EOL;
                    $this->sendPayload(1, $message['s']);
                    break;

                case 11:
                    echo "Received heartbeat ACK." . PHP_EOL;
                    if (!$this->hasSentIdentity) {
                        echo "Sending app identification" . PHP_EOL;
                        $this->sendPayload(2, [
                            'token'      => env('BOT_TOKEN'),
                            'properties' => [
                                '$os' => 'linux',
                            ]
                        ]);
                        $this->hasSentIdentity = true;
                    }
                    break;

                default:
                    if (!$message) {
                        echo "Received empty message." . PHP_EOL;
                        break;
                    }
                    print_r($message);
                    die();

            }
        });

        $this->periodicCallback(function () {
            $this->sendPayload(1, []);
        }, 60);
    }

    public function on(string $event, callable $callback): void
    {
        $this->eventList[$event][] = $callback;
    }

    protected function sendPayload(int $opcode, $data)
    {
        $this->sendMessage(json_encode([
            'op' => $opcode,
            'd'  => $data
        ]));
    }

}