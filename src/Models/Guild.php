<?php

namespace judahnator\DiscordWebsocket\Models;


use judahnator\DiscordHttpWrapper\Guild as BaseGuild;
use judahnator\DiscordWebsocket\Cache;

class Guild extends BaseGuild
{

    public static function find(int $GuildID)
    {
        return Cache::remember("guild.{$GuildID}", function() use ($GuildID) {
            return new static($GuildID);
        });
    }

}