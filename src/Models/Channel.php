<?php

namespace judahnator\DiscordWebsocket\Models;


use judahnator\DiscordHttpWrapper\Channel as BaseChannel;
use judahnator\DiscordWebsocket\Cache;

class Channel extends BaseChannel
{

    public static function find(int $ChannelID)
    {
        return Cache::remember("Channel.{$ChannelID}", function() use ($ChannelID) {
            return new static($ChannelID);
        });
    }

}