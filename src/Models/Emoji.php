<?php

namespace judahnator\DiscordWebsocket\Models;


/**
 * Class Emoji
 * @property string $name
 * @property int|null $id
 * @property bool $animated
 * @package judahnator\DiscordWebsocket\Models
 */
class Emoji
{

    private $attributes = [];

    /**
     * Emoji constructor.
     * @param string $name
     * @param int|null $id
     * @param bool $animated
     */
    public function __construct(string $name, $id = null, bool $animated = false)
    {
        $this->attributes = [
            'name' => $name,
            'id' => $id,
            'animated' => $animated
        ];
    }

    public function __get($name)
    {
        return $this->attributes[$name] ?? null;
    }

}