<?php

namespace judahnator\DiscordWebsocket\Models;


use judahnator\DiscordHttpWrapper\Author as BaseAuthor;
use judahnator\DiscordWebsocket\Cache;

class Author extends BaseAuthor
{

    public static function find(int $AuthorID)
    {
        return Cache::remember("author.{$AuthorID}", function() use ($AuthorID) {
            return new static($AuthorID);
        });
    }

}