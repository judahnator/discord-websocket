<?php

namespace judahnator\DiscordWebsocket\Functions;

use Dotenv\Dotenv;

function env(string $envVar)
{
    static $dotenv = null;
    if (is_null($dotenv)) {
        $dotenv = new Dotenv(realpath(__DIR__.'/../../'));
        $dotenv->load();
    }
    return getenv($envVar);
}