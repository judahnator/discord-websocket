<?php

namespace judahnator\DiscordWebsocket;


use judahnator\Option\Drivers\MemoryDriver;
use judahnator\Option\Option;

class Cache
{

    /**
     * Caches a given key in the option store temporarily.
     * The duration defaults to 5 minutes.
     *
     * @param string $key
     * @param callable $ifNotFound
     * @param \DateInterval|null $duration
     * @return mixed
     */
    public static function remember(string $key, callable $ifNotFound, \DateInterval $duration = null)
    {
        static $option = null;
        if (is_null($option)) {
            $option = new Option(new MemoryDriver());
        }
        if (is_null($duration)) {
            $duration = new \DateInterval('PT5M');
        }
        if (!$option->has($key) || $option->get($key)['expires'] < new \DateTime()) {
            $option->set($key, [
                'expires' => (new \DateTime())->add($duration),
                'value' => $ifNotFound()
            ]);
        }
        return $option->get($key)['value'];
    }

}